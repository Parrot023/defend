# Defend

A game written in Python3 using the library 'arcade' found at: http://arcade.academy/

Control your spaceship using the arrow keys. Fire with the space key.

The graphics use in the game come from [kenney.nl](https://kenney.nl/).

# Installation
To run the game, you must install the needed Python libraries (Currently one):
```
pip3 install -r requirements.txt
```

# To do
- [ ] Explosions: Random size and length
- [ ] Add enemy types (A single type of enemy is quite boring)
- [ ] Difficulty rises proportionally to number of Enemies in game
- [ ] Fire rate? Limited number of shots on screen?
- [ ] Animation: Player dead
- [ ] Weapon types (Various explosion sizes. Danger to self)
- [ ] Levels (With waves?)
- [ ] Random spawn points for enemies? Some could be friendly? -> (% bonus) (Don't kill friends!)
- [ ] Power ups in random zone. Player would need to be able to move
- [ ] Player rotates to new angle instead if instant change
- [ ] Meaningfull graphics for power ups. Currently colored pills
- [ ] Add likelihood to power up types. Some should be more likely than others.
- [ ] Add support for gamepad
- [ ] Show number of hyperspace player has
- [ ] Stars? in background. Move slowly or game will "break" (Illusion wise).
- [ ] Muliplayer (On same machine).
- [ ] Different Enemies (Bosses)
- [ ] Put game configuration in config file
- [X] Maybe hyperspace charges up, instead of beeing a power up which is picked up?
- [X] Move around in a grid? Need power to move in grid = moves slowly in grid
- [X] Animation: Enemy dead
- [x] Ammo for player
- [x] Power ups (Pick something up. What happens? Ammo? Shield?)

# Contributors
* Andreas L. Johannesson (Parrot023)
* Ludvig Erdmann (LudvigDK)
* Laurits E. Overgaard Christiansen (laurits1208)
* Sylvester Reinbothe (Lord Kermit)
* Eigil Skytte (eigil.skytte)
